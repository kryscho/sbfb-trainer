/*
 *  Copyright 2014-2020 Matthias Wimmer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wimmerinformatik.trainer.data;

import java.io.IOException;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import android.content.res.XmlResourceParser;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class QuestionaireReader {
    private QuestionaireReader() {
        // disable construction
    }

    public static List<TopicQuestions> readFrom(XmlResourceParser x) {
        try {
            return tryReadFrom(x);
        } catch (final IOException ioe) {
            throw new RuntimeException(ioe);
        } catch (final XmlPullParserException ppe) {
            throw new RuntimeException(ppe);
        }
    }

    public static List<TopicQuestions> tryReadFrom(
        XmlResourceParser x)
        throws IOException, XmlPullParserException {

        final List<TopicQuestions> res = new LinkedList<TopicQuestions>();

        int eventType = x.getEventType();
        TopicQuestions.TopicQuestionsBuilder tqb = TopicQuestions.builder();
        int currentTopicId = 0;
        Question.QuestionBuilder currentQuestion = null;
        boolean expectingAnswer = false;
        boolean expectingQuestion = false;
        int index = 0;
        while (eventType != XmlPullParser.END_DOCUMENT) {
            switch (eventType) {
                case XmlPullParser.START_TAG:
                    final String tagName = x.getName();
                    if ("topic".equals(tagName)) {
                        currentTopicId =
                            x.getAttributeIntValue(null, "id", 0);
                        final String name =
                            x.getAttributeValue(null, "name");
                        tqb.meta(Topic.builder()
                                 .id(currentTopicId)
                                 .index(index++)
                                 .name(name)
                                 .build());
                    } else if ("question".equals(tagName)) {
                        currentQuestion = Question.builder();
                        currentQuestion.id(
                            x.getAttributeIntValue(null, "id", 0));
                        currentQuestion.reference(
                            x.getAttributeValue(null, "reference"));
                        currentQuestion.nextTime(new Date());
                        currentQuestion.topicId(currentTopicId);
                    } else if ("text".equals(tagName)) {
                        expectingQuestion = true;
                    } else if ("correct".equals(tagName)) {
                        expectingAnswer = true;
                    } else if ("incorrect".equals(tagName)) {
                        expectingAnswer = true;
                    }
                    break;
                case XmlPullParser.TEXT:
                    if (expectingQuestion) {
                        currentQuestion.questionText(x.getText());
                        expectingQuestion = false;
                    }
                    if (expectingAnswer) {
                        currentQuestion.answer(x.getText());
                        expectingAnswer = false;
                    }
                case XmlPullParser.END_TAG:
                    final String endTagName = x.getName();
                    if ("topic".equals(endTagName)) {
                        res.add(tqb.build());
                        tqb = TopicQuestions.builder();
                    } else if ("question".equals(endTagName)) {
                        tqb.question(currentQuestion.build());
                        currentQuestion = null;
                    }
                    break;
            }
            x.next();
            eventType = x.getEventType();
        }

        return res;
    }
}
